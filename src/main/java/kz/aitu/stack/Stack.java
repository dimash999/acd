package kz.aitu.stack;

public class Stack {
    Node top;

    public int pop(){
        Node popped;
        popped = top;
        top = top.next;
        return popped.inf;
    }

    public void push(int data) {
        Node newNode = new Node(data);
        if (top == null) {
            top = newNode;
        } else {
            newNode.next = top;
            top = newNode;
        }
    }

    public boolean empty() {
        if (top == null) {
            return true;
        } else {
            return false;
        }
    }

    public int size() {
        Node cur = top;
        int sum=0;
        if (top == null) {
            return 0;
        } else {
            while (cur != null) {
                cur = cur.next;
                sum++;
            }
        }
        return sum;
    }

    public int top() {
        if (top == null) {
            return 0;
        } else {
            return top.inf;
        }
    }
}


