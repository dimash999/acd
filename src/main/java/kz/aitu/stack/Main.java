    package kz.aitu.stack;

    public class Main {
        public static void main(String[] args) {
            Stack Stack = new Stack();
            Stack.push(1);
            Stack.push(2);
            Stack.push(3);

            System.out.print("print stack ");
            Node cur = Stack.top;
            while (cur != null) {
                System.out.print(cur.inf + " ");
                cur = cur.next;
            }
            System.out.println();

            System.out.println("pop: " + Stack.pop());
            System.out.println("empty? " + Stack.empty());
            System.out.println("size: " + Stack.size());

            System.out.print("print stack");
            Node cur2 = Stack.top;
            while (cur2 != null) {
                System.out.print(cur2.inf + " ");
                cur2 = cur2.next;
            }
            System.out.println();
        }
    }


