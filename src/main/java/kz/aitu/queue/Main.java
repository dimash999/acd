package kz.aitu.queue;

public class Main {
    public static void main(String[] args) {
        queue queue = new queue();

        queue.add(1);
        queue.add(2);
        queue.add(3);

        System.out.println("peek: " + queue.peek());
        queue.remove(1);

        queue.poll();

        System.out.println("size: " + queue.size());

        System.out.println("is stack empty? " + queue.empty());


    }
}

