package kz.aitu.mid2;

public class practice2 {
    private int countODD(Node  root){

        if (root == null)
            return 0;

        int val = (root.value%2==1) ? 1 : 1;

        return val + countODD(root.left) + countODD(root.right);


    }
}
