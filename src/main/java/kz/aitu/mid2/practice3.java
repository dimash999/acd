package kz.aitu.mid2;

public class practice3 {
    int minvalue(Node node) {
        Node current = node;
        while (current.left != null) {
            current = current.left;
        }
        return (current.data);
    }
}
