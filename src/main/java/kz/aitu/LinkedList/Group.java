package kz.aitu.LinkedList;

public class Group {
    private Block head;
    private Block tail;

    public Block getHead() {
        return head;
    }

    public Block getTail() {
        return tail;
    }

    public void setHead(Block head) {
        this.head = head;
    }

    public void setTail(Block tail) {
        this.tail = tail;
    }

    public void addBlock(Block block){
        if (head==null){
            head= block;
            tail= block;
        } else {
            tail.setNextBlock(block);
            tail =block;
        }
    }

    public void Inserttostart(int value){
        Block newblock = new Block(value);
        newblock.setNextBlock(head);
        head = newblock;
        System.out.println("inserted to top");
    }

    public void Inserttopos(int index, int value){
        Block newblock = new Block(value);
        Block currentB = head, prev = null;
        if (index == 0 && currentB!=null){
            newblock.setNextBlock(head);
            head = newblock;
            System.out.println(value + " inserted to poition "+ index);
        } else {
            int counter = 0;
            while (currentB!=null){
                if (index == counter){
                    prev.setNextBlock(newblock);
                    newblock.setNextBlock(currentB);
                    System.out.println(value + " inserted to poition "+ index);
                    break;
                }
                prev = currentB;
                currentB = currentB.getNextBlock();
                counter++;
            }
            if (currentB==null){
                System.out.println("there is no value before your index");
            }
        }
    }

    public void DeleteValue(int value){
        Block currentB = head, prev = null;
        if (value == currentB.getValue()){
            head = currentB.getNextBlock();
            System.out.println("value deleted");
        } else {
            while (currentB!=null && currentB.getValue()!=value){
                prev = currentB;
                currentB = currentB.getNextBlock();
            }
            if (currentB==null){
                System.out.println("cant find value");
            } else {
                prev.setNextBlock(currentB.getNextBlock());
                System.out.println("value deleted");
            }
        }
    }

    public void DeleteValueofIndex(int index){
        Block currentB = head, prev = null;
        if (index == 0){
            head = currentB.getNextBlock();
            System.out.println("value in index "+index+" deleted");
        } else{
            int counter = 0;
            while (currentB!=null){
                if (counter==index){
                    prev.setNextBlock(currentB.getNextBlock());
                    System.out.println("value in index "+index+" deleted");
                    break;
                }
                prev = currentB;
                currentB = currentB.getNextBlock();
                counter++;
            }
            if (currentB==null){
                System.out.println("index is not declared");
            }
        }
    }

    public int get(int index){
        Block currentB = head;
        int counter = 0;
        while (currentB!=null){
            if (counter == index){
                return currentB.getValue();
            }
            currentB = currentB.getNextBlock();
            counter++;
        }
        return 0;
    }
}
