package kz.aitu.LinkedList;

public class Block {
    private int value;
    private Block nextBlock;

    public Block(int value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNextBlock(Block nextBlock) {
        this.nextBlock = nextBlock;
    }

    public int getValue() {
        return value;
    }

    public Block getNextBlock() {
        return nextBlock;
    }
}
