package kz.aitu.LinkedList;

import java.util.Scanner;

public class Main {
    public static void reversedList(Group group){
        Block block = group.getHead();
        int y =-1;
        while (block!=null){
            block = block.getNextBlock();
            y++;
        }
        for (int i = y;i>=0;i--){
            System.out.print(group.get(i) + " ");
        }
    }

    public static int printList(Group group){
        Block block = group.getHead();
        int y =0;
        while (block!=null){
            System.out.print(block.getValue() + " ");
            block = block.getNextBlock();
            y++;
        }
        System.out.println();
        return y;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Group group1 = new Group();
        for (int i=0;i<5;i++){
            System.out.print("write number "+i+" : ");
            int a = scanner.nextInt();
            group1.addBlock(new Block(a));
        }
        System.out.println(printList(group1));

        group1.Inserttostart(15);
        System.out.println(printList(group1));

        group1.Inserttopos(2, 11);
        System.out.println(printList(group1));

        group1.DeleteValue(1);
        System.out.println(printList(group1));

        group1.DeleteValueofIndex(3);
        System.out.println(printList(group1));

        reversedList(group1);
    }
}
