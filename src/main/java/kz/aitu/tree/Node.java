package kz.aitu.tree;

public class Node {
    public int data;
    public Node left;
    public Node right;

    public Node(int data){
        this.data=data;
        this.left=null;
        this.right=null;
    }

    public void insert(int data){
        if (data <= this.data) {
            if (left == null) {
                left = new Node(data);
            } else {
                left.insert(data);
            }
        } else {
            if (right == null) {
                right = new Node(data);
            } else {
                right.insert(data);
            }
        }
    }

    public boolean doesitcontain(int data){
        if (data == this.data) {
            return true;
        } else if (data < this.data) {
            if (left == null) {
                return false;
            } else {
                return left.doesitcontain(data);
            }
        } else {
            if (right == null) {
                return false;
            } else {
                return right.doesitcontain(data);
            }
        }
    }

    public void printInOrder() {
        if (left != null) {
            left.printInOrder();
        }
        System.out.print(data+" ");
        if(right != null) {
            right.printInOrder();
        }
    }
}
