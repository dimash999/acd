package kz.aitu.tree;

public class BinaryTree {
    public Node root;

    public BinaryTree(){
        this.root = null;
    }

    public void add(int data) {
        if (root == null) root = new Node(data);
        else root.insert(data);
    }

    public int root(){
        return root.data;
    }

    public boolean doesitcontain(int data){
        if (this.root()==data) return true;
        else {
            if (root.doesitcontain(data)) return true;
            else return false;
        }
    }

    public void printInOrder(){
        root.printInOrder();
    }
}
